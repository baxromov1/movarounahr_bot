import telebot
from django.http import JsonResponse
from django.views import View
from telebot.types import ReplyKeyboardMarkup, KeyboardButton, ReplyKeyboardRemove
from telebot import types
import logging
from app.models import TgUser, Admin, Info
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile
import time


logger = telebot.logger
telebot.logger.setLevel(logging.DEBUG)

BOT_NAME = '@ProductUzBot'
bot = telebot.TeleBot('1772698205:AAHhlgWWmUx7SdT6V5xGXr4GPQLI4LA48_Q')



class BotAPIView(View):
    def post(self, request):
        json_string = request.body.decode('UTF-8')
        update = telebot.types.Update.de_json(json_string)
        bot.process_new_updates([update])
        return JsonResponse({'code': 200})


@bot.message_handler(content_types=['video'])
def send_video_id(message):
    file_id = message.video.file_id
    info = Info.objects.last()
    info.video_id = message.video.file_id
    info.save()
    print(file_id)
    bot.send_message(message.from_user.id, file_id)


@bot.message_handler(commands=['adminvideo'], func=lambda message: Admin.objects.filter(chat_id=message.from_user.id).exists())
def admin_video(message):
    bot.send_message(message.from_user.id, "video yubor")


@bot.message_handler(commands=['start'])
def start(message):
    info = Info.objects.last()
    bot.send_video(message.from_user.id, info.video_id, caption=info.text)

    time.sleep(2)
    text = "Номерни юборинг"
    TgUser.objects.get_or_create(pk=message.from_user.id)
    rkm = ReplyKeyboardMarkup(True)
    rkm.add(KeyboardButton('Номерни юбориш', request_contact=True))
    bot.send_message(message.from_user.id, text, reply_markup=rkm)
    bot.register_next_step_handler(message, enter_name)


def enter_name(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        if message.contact:
            user = TgUser.objects.get(pk=message.from_user.id)
            user.phone = message.contact.phone_number
            user.save()
            text = 'Исм фамилянгизни киритинг'
            rkm = types.ReplyKeyboardRemove()
            bot.send_message(message.chat.id, text, reply_markup=rkm)
            bot.register_next_step_handler(message, enter_address)
        else:
            rkm = ReplyKeyboardMarkup(True)
            rkm.add(KeyboardButton('Номерни юбориш', request_contact=True))
            bot.send_message(message.chat.id, "Номерни юбориш", reply_markup=rkm)
            bot.register_next_step_handler(message, enter_name)


def enter_address(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        user.name = message.text
        user.save()
        text = 'Уй манзилингизни киритинг'
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, enter_age)


# def enter_family_status(message):
#     if message.text == '/start':
#         bot.clear_step_handler(message)
#         start(message)
#     else:
#         user = TgUser.objects.get(pk=message.from_user.id)
#         user.address = message.text
#         user.save()
#         text = 'Оилавий шароитингиз'
#         rkm = types.ReplyKeyboardMarkup(resize_keyboard=True)
#         rkm.add('Оиласиз', 'Оилавий')
#         bot.send_message(message.chat.id, text, reply_markup=rkm)
#         bot.register_next_step_handler(message, enter_age)


def enter_age(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        user.family_status = message.text
        user.save()
        # rkm = types.ReplyKeyboardRemove()
        text = 'Ёшингизни киртинг'
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, enter_photo)


def enter_photo(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        user.age = message.text
        user.save()
        text = 'Расм'
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, get_photo)


def get_photo(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        if message.photo:
            if message.photo:
                user = TgUser.objects.get(pk=message.from_user.id)
                file_id = message.photo[-1].file_id
                file_info = bot.get_file(file_id)
                downloaded_file = bot.download_file(file_info.file_path)
                img_temp = NamedTemporaryFile(delete=True)
                img_temp.write(downloaded_file)
                img_temp.flush()

                user.image.save(f"image-{message.from_user.id}.jpg", File(img_temp), save=True)
                user.save()

                get_main(message)
            else:
                text = 'Расм юборинг'
                bot.send_message(message.chat.id, text)
                bot.register_next_step_handler(message, get_photo)
        elif message.document:
            if message.document.mime_type == 'image/png' or message.document.mime_type == 'image/jpg' or message.document.mime_type == 'image/jpeg':
                user = TgUser.objects.get(pk=message.from_user.id)
                image_type = message.document.mime_type.split('/')[-1]
                file_id = message.document.file_id
                file_info = bot.get_file(file_id)
                downloaded_file = bot.download_file(file_info.file_path)
                img_temp = NamedTemporaryFile(delete=True)
                img_temp.write(downloaded_file)
                img_temp.flush()
                user.image.save(f"image-{message.from_user.id}.{image_type}", File(img_temp), save=True)
                user.save()

                get_main(message)
            else:
                text = 'Расм юборинг'
                bot.send_message(message.chat.id, text)
                bot.register_next_step_handler(message, get_photo)
        else:
            text = 'Расм юборинг'
            bot.send_message(message.chat.id, text)
            bot.register_next_step_handler(message, get_photo)


def get_main(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        rkm = types.ReplyKeyboardMarkup(resize_keyboard=True, row_width=3)
        rkm.add('Чойхона ошпази','Fastfood ошпази','Ресторан ошпази',)
        rkm.add('Проект менежер', 'Карвинчи', 'Маркетолог', 'Хостес')
        rkm.add('Молячи', 'Бухгалтер', 'ИТ-мутахасис')
        rkm.add('Call-center оператори', 'Омборчи', 'Кассир', 'Хайдовчи')
        rkm.add('Қассоб', 'Фаррош', 'Идиш ювувчи', 'Реклама мутахасиси')
        rkm.add('Иш бошқарувчи', 'Завхоз')
        rkm.add('Официант ва официанткалар', 'HR', 'Қурилиш ишлари бўйича менежер')
        text = "Бўлимни танланг"
        bot.send_message(message.chat.id, text, reply_markup=rkm)


btn = [
    'Проект менежер', 'Карвинчи', 'Маркетолог', 'Хостес',
    'Чойхона ошпази','Fastfood ошпази','Ресторан ошпази',
    'Молячи', 'Бухгалтер', 'ИТ-мутахасис',
    'Call-center оператори', 'Омборчи', 'Кассир', 'Хайдовчи',
    'Қассоб', 'Фаррош', 'Идиш ювувчи', 'Реклама мутахасиси',
    'Иш бошқарувчи', 'Завхоз',
    'Официант ва официанткалар', 'HR', 'Қурилиш ишлари бўйича менежер',
]


@bot.message_handler(
    func=lambda message: message.text in btn)
def text_message(message):
    if message.text == 'Проект менежер':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = "---"
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        pm_2(message)
    elif message.text == 'Чойхона ошпази':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = "Ошпаз – бу таом тайёрлаш буйича мутахасис саналади яхши ошпазларни сехргар деб хам юритишади, чунки улар оддий масаллиқдан инсонларга хурсандчилик ва завқ бағишлай оладиган дурдона асар ярата оладилар, ошпазлар махсус рецепт асосида таом тайёрлайдилар лекин ўзларини дидлари билан саньат даражасида маромига етказиш хусусиятига эга."
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        choyxona_2(message)
    elif message.text == 'Fastfood ошпази':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = "Ошпаз – бу таом тайёрлаш буйича мутахасис саналади яхши ошпазларни сехргар деб хам юритишади, чунки улар оддий масаллиқдан инсонларга хурсандчилик ва завқ бағишлай оладиган дурдона асар ярата оладилар, ошпазлар махсус рецепт асосида таом тайёрлайдилар лекин ўзларини дидлари билан саньат даражасида маромига етказиш хусусиятига эга."
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        fastfood_2(message)
    elif message.text == 'Ресторан ошпази':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = "Ошпаз – бу таом тайёрлаш буйича мутахасис саналади яхши ошпазларни сехргар деб хам юритишади, чунки улар оддий масаллиқдан инсонларга хурсандчилик ва завқ бағишлай оладиган дурдона асар ярата оладилар, ошпазлар махсус рецепт асосида таом тайёрлайдилар лекин ўзларини дидлари билан саньат даражасида маромига етказиш хусусиятига эга."
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        restoran_2(message)
    elif message.text == 'Карвинчи':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = "Карвинчи – бу санатдир. Карвинчи инсонлар қўли гул ижодкор ва махоратли хисобланадилар"
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        karvinchi_2(message)
    elif message.text == 'Маркетолог':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = "Маркетоло–бу мутахассис ишлаб чикарилувчи/сотилувчи махсулотни бозордаги талабини анализ қилиш ва бу талабни ошириш учун тавсиялар берувчи мутахасси."
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        karvinchi_2(message)
    elif message.text == 'Хостес':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = "Хостес – бу компаниянинг юзи хисобланади, унинг асосий вазифалари мижозларни кутиб олиш, уларга қўшимча хизматлар хақида маълумот бериш, мижозларда саволлар ва муаммолар пайдо бўлган тақдирда уларни хал қила олиши керак. Ушбу касб эгалари бир неча тилни билиши, хушмуомила бўлиши ва истарали бўлишлари керак."
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        xostes_2(message)
    elif message.text == 'Молячи':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = "Молиячи(жамият маъносида молиячи) - бу катта бойликка егалик қиладиган ёки уни бошқарадиган муваффақиятли одам. Кенг дунёқарашга ега, ривожланган сезги, муҳим тажриба, интизомли ва ғайратли"
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        molyachi_2(message)
    elif message.text == 'Бухгалтер':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = " Бухгалтер – бу бухгалтерия хисобини юритиш, корхона ва муассасаларнинг молиявий ишларини қайд этиб бориш, назорат қилиш, жумладан ходимларга иш хаққи ёзиш, бухгалтерия хисоботи ва бошқа хисоботлар тузиш билан шуғулланувчи махсус шахс, одатда Бухгалтерияни бош хисобчи (Бухгалтер) бошқаради"
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        buxgalter_2(message)
    elif message.text == 'ИТ-мутахасис':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = "IT-мутахассис – чинакам ижодкор хисобланади. Ушбу мутахассис программалар, мобил иловалар, сайтлар яратишдан ташқари, янги ғоялар ва таклифлар беради. Ушбу мутахассис ўзи тайёрлаган ишини доимий текширади хатоларини тўғирлайди ва мукамаллаштиради"
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        it_mutaxasis_2(message)
    elif message.text == 'Call-center оператори':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = "Кол центр оператори – бу мутахассис ширин сўз, хушмуомила ва бир неча тилларда (узбек ва рус) равон гапира олувчи, компьютер ва ижтимоий тармоқлардан фаол фойдаланувчи, берилган саволларга ўз ваколати даражасида тўлиқ жавоб берувчи шахс."
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        call_center_operator_2(message)
    elif message.text == 'Омборчи':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = "Омборчи – бу касб эгаси компания омборига келган махсулотларни  қабул қилиб олиши, тартибига кўра жойлаштириши, керак бўлганда шу махсулотни хайдовчининг автомашинасига юклаб берувчи шахс."
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        omborchi_2(message)
    elif message.text == 'Кассир':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = "Кассир – бу касб эгаси хизмат ёки махсулот учун маблағни қабул қилиб олиш, тўланган сумма бўйича чек бериш билан шуғиланувчи, компьютердан мустақил фойдалана орлувчи шахс."
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        kassir_2(message)
    elif message.text == 'Хайдовчи':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = "Хайдовчи – бу касб эгаси хайдовчилик гувохномасига эга бўлиши керак, унинг вазифаси  компания авто машинасида ёки шахсий автомашинада, компаниянинг  юкларини ва ходимларни ташиш билан шуғиланади."
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        xaydovchi_2(message)
    elif message.text == 'Қассоб':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = "Қассоб – қассоб бу сўювчи ва гўшт билан савдо қилувчи шахс."
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        qassob_2(message)
    elif message.text == 'Фаррош':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = " Фаррош – тозалик  ишлари учун масъул шахс хисобланади."
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        farrosh_2(message)
    elif message.text == 'Идиш ювувчи':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = " Идиш ювувчи – буюмларни тозалиги учун  масъул шахс хисобланади."
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        idish_yuvuvchi_2(message)
    elif message.text == 'Реклама мутахасиси':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = "Реклама мутахасиси - бу смм сохасини билиши, Маркетингни тушуна олиши, Реклама сохасида ишлаган болиши, Интернетдан малумотлар топа оладигон мутахасис."
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        reklama_mutaxasisi_2(message)
    elif message.text == 'Иш бошқарувчи':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = " Иш бошкарувчи – менеджер корхона ва компания эгалари бўлмаган, махсус тайёргарлик кўрган, бошқаришнинг қонун қоидаларини чуқур билувчи малакали мутахассис,  менеджер ривожланган мамлакатларда махсус ижтимоий қатламни ташкил қилади."
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        ish_boshqaruvi_2(message)
    elif message.text == 'Завхоз':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = "Завхоз - хар қандай ташкилот йоки муассасада маъсул лавозим хисобланади. Завхознинг асосий маъсуляти ташкилотни иқтисодий ва техник қўллаб қувватлаш ва техник хмзмат кўрсатишдир."
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        zavhoz_2(message)
    elif message.text == 'Официант ва официанткалар':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = " Официант ва официанткалар – муваффақиятли иш учун официантга жисмоний чидамлилик, ҳаракатларни яхши мувофиқлаштириш, ривожланган визуал хотира, эътиборни тақсимлаш қобилияти, ижтимоий таъсирчанлик, стрессга чидамлилик, хушмуомалалик, дўстона муносабат, одоб-ахлоқ қоидаларини билиш ва арифметик маҳорат керак."
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        afitsant_va_afitsantkalar_2(message)
    elif message.text == 'HR':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = "HR – компанияда ходимларни бошқариш билан шуғилланиб, уларни ишга қабул қилган дақиқадан токи мехнат шартномаси бекор қилингунича бўлган барча жараёнларда иштирок этади."
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        xodimlar_bolimi_raxbari_2(message)
    elif message.text == 'Қурилиш ишлари бўйича менежер':
        user = TgUser.objects.get(pk=message.from_user.id)
        user.type = message.text
        user.save()
        text = "---"
        bot.send_message(message.from_user.id, text)
        time.sleep(2)
        qurilish_ishlari_boyicha_menijir_2(message)


# oshpaz
def restoran_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Бундан аввал қаерда ва қанча муддат ишлагансиз?"
        user.text = text
        user.save()
        rkm = ReplyKeyboardRemove(selective=None)
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, restoran_3)

def restoran_3(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Мехнат тажрибангиз қанча?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, restoran_4)

def restoran_4(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Мехнат тажрибангиз қанча?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        rkm = types.ReplyKeyboardMarkup(resize_keyboard=True)
        rkm.row("йирик корхонада", "чойхонада", "уй шароитида")
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, finish)


def choyxona_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        restoran_2(message)


def fastfood_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        restoran_2(message)

# pm
def pm_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Бундан аввал қаерда ва қанча муддат ишлагансиз?"
        user.text = text
        user.save()
        rkm = ReplyKeyboardRemove(selective=None)
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, pm_3)


def pm_3(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Мехнат тажрибангиз қанча?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, pm_4)


def pm_4(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Мехнат тажрибангиз қанча?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        # rkm = types.ReplyKeyboardMarkup(resize_keyboard=True)
        # rkm.row("йирик корхонада", "чойхонада", "уй шароитида")
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, finish)

# karvinchi


def karvinchi_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Бундан аввал қаерда ва қанча муддат ишлагансиз?"
        user.text = text
        user.save()
        rkm = ReplyKeyboardRemove(selective=None)
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, karvinchi_3)


def karvinchi_3(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Сўнги иш хаққингиз қанча бўлган??"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, karvinchi_4)


def karvinchi_4(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Мехнат тажрибангиз қанча?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, karvinchi_5)


def karvinchi_5(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Сиз қарор қабул қилишда ўзингизга таянасизми?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        rkm = types.ReplyKeyboardMarkup(resize_keyboard=True)
        rkm.row("ха", "йоқ")
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, finish)

# marketolog


def marketolog_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Қайси тилларда мустақил гаплаша оласиз?"
        user.text = text
        user.save()
        rkm = ReplyKeyboardRemove(selective=None)
        bot.send_message(message.chat.id, text,reply_markup=rkm)
        bot.register_next_step_handler(message, marketolog_3)


def marketolog_3(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "СММ сохасида қандай тажрибага эгасиз?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, marketolog_4)


def marketolog_4(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Аналитик маркетинг сохасида қандай тажрибага эгасиз?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, marketolog_5)


def marketolog_5(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Рекламанинг қайси турларидан фаол фойдаланасиз?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, marketolog_6)


def marketolog_6(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Маркетинг бўйича стратегия тузиш тажрибангиз борми?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        rkm = types.ReplyKeyboardMarkup(resize_keyboard=True)
        rkm.row("ха", "йўқ")
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, marketolog_7)


def marketolog_7(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Маркетинг бўйича сўнги ўқиган китобингиз?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        rkm = ReplyKeyboardRemove(selective=None)
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, finish)

#xostes


def xostes_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Қайси тилларда мустақил гаплаша оласиз?"
        user.text = text
        user.save()
        rkm = ReplyKeyboardRemove(selective=None)
        bot.send_message(message.chat.id, text,reply_markup=rkm)
        bot.register_next_step_handler(message, xostes_3)


def xostes_3(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Бундан аввал қаерда ва қанча муддат ишлагансиз?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, xostes_4)


def xostes_4(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Мехнат тажрибангиз қанча?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, xostes_5)


def xostes_5(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Сўнги иш хаққингиз қанча бўлган?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, xostes_6)


def xostes_6(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Сиз қарор қабул қилишда ўзингизга таянасизми?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        rkm = types.ReplyKeyboardMarkup(resize_keyboard=True)
        rkm.row("xa", "yoq")
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, finish)


#molyachi


def molyachi_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Қайси тилларда мукаммал сухбатлаша оласиз?"
        user.text = text
        user.save()
        rkm = ReplyKeyboardRemove(selective=None)
        bot.send_message(message.chat.id, text,reply_markup=rkm)
        bot.register_next_step_handler(message, molyachi_3)


def molyachi_3(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Микрософт офис дастурлари билан иш тажрибага эгамисиз?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, molyachi_4)


def molyachi_4(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Молия ҳисобидан ҳабарингиз борми?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, finish)


# buxgalter


def buxgalter_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Қайси тилларда мустақил гаплаша оласиз?"
        user.text = text
        user.save()
        rkm = ReplyKeyboardRemove(selective=None)
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, buxgalter_3)


def buxgalter_3(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "1С программасидан мустақил фойдалана оласизми?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        rkm = types.ReplyKeyboardMarkup(resize_keyboard=True)
        rkm.row("xa", "йўқ")
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, buxgalter_4)


def buxgalter_4(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Excel программасидан мустақил фойдалана оласизми?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        rkm = types.ReplyKeyboardMarkup(resize_keyboard=True)
        rkm.row("xa", "йўқ")
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, buxgalter_5)


def buxgalter_5(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Ойлик хисоботларни топшириш тажрибангиз борми?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        rkm = types.ReplyKeyboardMarkup(resize_keyboard=True)
        rkm.row("xa", "йўқ")
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, buxgalter_6)


def buxgalter_6(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Soliq.uz сайтида хисоботлар топшира оласизми?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        rkm = types.ReplyKeyboardMarkup(resize_keyboard=True)
        rkm.row("xa", "йўқ")
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, buxgalter_7)


def buxgalter_7(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Бундан аввал қаерда ва қанча муддат ишлагансиз?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        rkm = ReplyKeyboardRemove(selective=None)
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, buxgalter_8)


def buxgalter_8(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Мехнат тажрибангиз қанча?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, buxgalter_9)


def buxgalter_9(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Сўнги иш хаққингиз қанча бўлган?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, finish)


# it_mutaxasis


def it_mutaxasis_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Қайси тилларда мустақил гаплаша оласиз?"
        user.text = text
        user.save()
        rkm = ReplyKeyboardRemove(selective=None)
        bot.send_message(message.chat.id, text,reply_markup=rkm)
        bot.register_next_step_handler(message, it_mutaxasis_3)


def it_mutaxasis_3(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "ИТ сохасида нималардан ҳабарингиз бор?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, it_mutaxasis_4)


def it_mutaxasis_4(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Ўз қолингиз билан тузган қандайдир дастурингиз борми? (Nomi, va u qanday dastur)?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, finish)


# call_center_operator


def call_center_operator_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Қайси тилда мустақил сухбатлаша оласиз?"
        user.text = text
        user.save()
        rkm = ReplyKeyboardRemove(selective=None)
        bot.send_message(message.chat.id, text,reply_markup=rkm)
        bot.register_next_step_handler(message, call_center_operator_3)


def call_center_operator_3(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Бундан аввал қаерда ва канча мудат ишлагансиз?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, call_center_operator_4)


def call_center_operator_4(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Сўнги ишингизда қанча иш хаққи олгансиз?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, call_center_operator_5)


def call_center_operator_5(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Сиз қарор қабул қилишда ўзингизга таянасизми?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, call_center_operator_6)


def call_center_operator_6(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Сиз таваккал қилишни ёқтирасизми?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, finish)


# omborchi


def omborchi_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Бундан аввал қаерда ва қанча муддат ишлагансиз?"
        user.text = text
        user.save()
        rkm = ReplyKeyboardRemove(selective=None)
        bot.send_message(message.chat.id, text,reply_markup=rkm)
        bot.register_next_step_handler(message, omborchi_3)


def omborchi_3(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Сўнги иш хаққингиз қанча бўлган?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, omborchi_4)


def omborchi_4(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Сиз қарор қабул қилишда ўзингизга таянасизми?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        rkm = types.ReplyKeyboardMarkup(resize_keyboard=True)
        rkm.row("xa", "йўқ")
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, finish)


def omborchi_5(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Сиз таваккал қилишни ёқтирасизми?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        rkm = types.ReplyKeyboardMarkup(resize_keyboard=True)
        rkm.row("xa", "йўқ")
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, omborchi_6)


def omborchi_6(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Барча ютуқларингизга ўз кучингиз билан эришганмисиз?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        rkm = types.ReplyKeyboardMarkup(resize_keyboard=True)
        rkm.row("xa", "йўқ")
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, omborchi_7)


def omborchi_7(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Энг яхши натижа бу жамоа билан бирга эришан натижами?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        rkm = types.ReplyKeyboardMarkup(resize_keyboard=True)
        rkm.row("xa", "йўқ")
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, finish)


# kassir



def kassir_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Бундан аввал қаерда ва қанча муддат ишлагансиз?"
        user.text = text
        user.save()
        rkm = ReplyKeyboardRemove(selective=None)
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, kassir_3)


def kassir_3(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Сўнги иш хаққингиз қанча бўлган??"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, kassir_4)


def kassir_4(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Мехнат тажрибангиз қанча?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, kassir_5)


def kassir_5(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Сиз қарор қабул қилишда ўзингизга таянасизми??"
        user.text += f'{message.text}\n{text} : '
        user.save()
        rkm = types.ReplyKeyboardMarkup(resize_keyboard=True)
        rkm.row("ха", "йоқ")
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, finish)


# xaydovchi


def xaydovchi_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Бундан аввал қаерда ва қанча муддат ишлагансиз?"
        user.text = text
        user.save()
        rkm = ReplyKeyboardRemove(selective=None)
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, xaydovchi_3)


def xaydovchi_3(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Мехнат тажрибангиз қанча?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, xaydovchi_4)


def xaydovchi_4(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Хайдовчилик гувохномангиз тоифаси кандай?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, xaydovchi_5)


def xaydovchi_5(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Шахсий мошинангизга эгамисиз?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, xaydovchi_6)


def xaydovchi_6(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Андижон шахрини яхши биласизми?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, xaydovchi_4)

# qassob


def qassob_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Бундан аввал қаерда ва қанча муддат ишлагансиз?"
        user.text = text
        user.save()
        rkm = ReplyKeyboardRemove(selective=None)
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, qassob_3)


def qassob_3(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Сўнги иш хаққингиз қанча бўлган??"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, qassob_4)


def qassob_4(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Мехнат тажрибангиз қанча?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, qassob_5)


def qassob_5(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Сиз қарор қабул қилишда ўзингизга таянасизми??"
        user.text += f'{message.text}\n{text} : '
        user.save()
        rkm = types.ReplyKeyboardMarkup(resize_keyboard=True)
        rkm.row("ха", "йоқ")
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, finish)


# farrosh


def farrosh_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Бундан аввал қаерда ва қанча муддат ишлагансиз?"
        user.text = text
        user.save()
        rkm = ReplyKeyboardRemove(selective=None)
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, farrosh_3)


def farrosh_3(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Мехнат тажрибангиз қанча?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, farrosh_4)


def farrosh_4(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Сўнги иш хаққингиз қанча бўлган??"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, finish)


# idish_yuvuvchi


def idish_yuvuvchi_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Бундан аввал қаерда ва қанча муддат ишлагансиз?"
        user.text = text
        user.save()
        rkm = ReplyKeyboardRemove(selective=None)
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, idish_yuvuvchi_3)


def idish_yuvuvchi_3(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Мехнат тажрибангиз қанча?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, idish_yuvuvchi_4)


def idish_yuvuvchi_4(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Сўнги иш хаққингиз қанча бўлган??"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, finish)


# reklama_mutaxasisi


def reklama_mutaxasisi_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Бундан аввал қаерда ва қанча муддат ишлагансиз?"
        user.text = text
        user.save()
        rkm = ReplyKeyboardRemove(selective=None)
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, reklama_mutaxasisi_3)


def reklama_mutaxasisi_3(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Сўнги иш хаққингиз қанча бўлган??"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, reklama_mutaxasisi_4)


def reklama_mutaxasisi_4(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Мехнат тажрибангиз қанча?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, reklama_mutaxasisi_5)


def reklama_mutaxasisi_5(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Сиз қарор қабул қилишда ўзингизга таянасизми??"
        user.text += f'{message.text}\n{text} : '
        user.save()
        rkm = types.ReplyKeyboardMarkup(resize_keyboard=True)
        rkm.row("ха", "йоқ")
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, finish)


# ish_boshqaruvi


def ish_boshqaruvi_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        reklama_mutaxasisi_2(message)


# zavhoz


def zavhoz_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        reklama_mutaxasisi_2(message)


# afitsant_va_afitsantkalar


def afitsant_va_afitsantkalar_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        reklama_mutaxasisi_2(message)


# xodimlar_bolimi_raxbari
def xodimlar_bolimi_raxbari_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Маълумотингиз қандай?"
        user.text = text
        user.save()
        rkm = ReplyKeyboardMarkup(resize_keyboard=True)
        rkm.row("олий", "ўрта")
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, xodimlar_bolimi_raxbari_3)


def xodimlar_bolimi_raxbari_3(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        if message.text == "олий":
            user = TgUser.objects.get(pk=message.from_user.id)
            text = "Қайси олийгохни якунлагансиз?"
            user.text += f'{message.text}\n{text} : '
            user.save()
            rkm = types.ReplyKeyboardRemove()
            bot.send_message(message.chat.id, text, reply_markup=rkm)
            bot.register_next_step_handler(message, oliy_2)
        elif message.text == "ўрта":
            user = TgUser.objects.get(pk=message.from_user.id)
            text = "Мутахсслигингиз?"
            user.text += f'{message.text}\n{text} : '
            user.save()
            rkm = types.ReplyKeyboardRemove()
            bot.send_message(message.chat.id, text, reply_markup=rkm)
            bot.register_next_step_handler(message, xodimlar_bolimi_raxbari_4)


def oliy_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Мутахсслигингиз?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, xodimlar_bolimi_raxbari_4)



def xodimlar_bolimi_raxbari_4(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Компьютердан мустақил фойдалана оласизми?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        rkm = ReplyKeyboardMarkup(resize_keyboard=True)
        rkm.row("ха", "йоқ")
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, xodimlar_bolimi_raxbari_5)


def xodimlar_bolimi_raxbari_5(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Иш малакангиз қанча?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        rkm = types.ReplyKeyboardRemove()
        bot.send_message(message.chat.id, text, reply_markup=rkm)
        bot.register_next_step_handler(message, xodimlar_bolimi_raxbari_6)


def xodimlar_bolimi_raxbari_6(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Бундан аввал қаерда ва қанча муддат ишлагансиз?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, xodimlar_bolimi_raxbari_7)



def xodimlar_bolimi_raxbari_7(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        user = TgUser.objects.get(pk=message.from_user.id)
        text = "Сўнги ишингизда қанча иш хаққи олгансиз?"
        user.text += f'{message.text}\n{text} : '
        user.save()
        bot.send_message(message.chat.id, text)
        bot.register_next_step_handler(message, finish)


# qurilish_ishlari_boyicha_menijir


def qurilish_ishlari_boyicha_menijir_2(message):
    if message.text == '/start':
        bot.clear_step_handler(message)
        start(message)
    else:
        reklama_mutaxasisi_2(message)


# finish
def finish(message):
    user = TgUser.objects.get(pk=message.from_user.id)
    user.text += f'{message.text}'
    user.save()
    text = "Сўровномани тўлдирганиз учун рахмат тез орада сиз билан боғланамиз!!!"
    info = f'Исм: _{user.name}_ \n' \
           f'Телефон: _{user.phone}_ \n' \
           f"Ёналиш: _{user.type}_ \n" \
           f'Манзил: _{user.address}_ \n' \
           f'Саволга жавоблар: \n{user.text}'

    rkm = ReplyKeyboardMarkup(True)
    rkm.add('/start')
    bot.send_message(message.from_user.id, text, reply_markup=rkm)
    admins = Admin.objects.all()

    for admin in admins:
        print(admin.chat_id)
        bot.send_photo(admin.chat_id, user.image, info, parse_mode='Markdown')
